import XMonad
import XMonad.Config.Desktop
import System.IO

-- import XMonad.Util.Run
import XMonad.Util.EZConfig(additionalKeysP)
import XMonad.Util.Run (runProcessWithInput, safeSpawn, spawnPipe)
import XMonad.Util.SpawnOnce
-- import XMonad.Util.Ungrab
import XMonad.Util.NamedScratchpad

import XMonad.Hooks.SetWMName
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.StatusBar
import XMonad.Hooks.StatusBar.PP


import XMonad.Layout.NoBorders   ( noBorders, smartBorders)
import XMonad.Layout.ToggleLayouts
import XMonad.Layout.ThreeColumns
import XMonad.Layout.Spacing
import XMonad.Layout.LayoutModifier
import XMonad.Layout.ResizableTile (MirrorResize(MirrorExpand, MirrorShrink), ResizableTall(..))
import XMonad.Layout.LayoutCombinators
import XMonad.Layout.Magnifier ( magnifiercz' )
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(FULL))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))

import XMonad.Actions.UpdatePointer
import XMonad.Actions.SpawnOn 
import XMonad.Actions.EasyMotion (selectWindow, EasyMotionConfig(..), textSize)
import XMonad.Actions.CopyWindow

import qualified XMonad.StackSet as W
import Data.Colour.Names (yellow)

myTerminal :: String
myTerminal = "kitty"

myEmacs :: String
myEmacs = "emacsclient -c -a 'emacs' "

myStartupHook :: X ()
myStartupHook = do
        setWMName "LG3D"
        spawnOnce "nitrogen --restore"
        spawnOnce "picom"
        spawnOnce "blueman-applet"
        spawnOnce "nm-applet"
        spawnOnce "kdeconnect-indicator"
        spawnOnce "trayer --edge top --monitor 0 --align right --widthtype percent --width 8 --padding 6 --SetDockType true --SetPartialStrut true --expand true --transparent true --alpha 0 --tint 0x000000 --height 16"
        spawnOnce "redshift-gtk"
        spawnOnce "xfce4-power-manager"
        -- spawnOnce "dropbox"
        spawnOnce "internxt-drive"
        spawnOnce "clipmenud"
        spawnOnce "/usr/bin/emacs --daemon"
        spawnOnce "xrandr --dpi 96"
        spawnOnce ( myTerminal ++ " -e xmodmap -e 'keycode 29 = y' && xmodmap -e 'keycode 52 = z'")
        spawnOnce "setxkbmap -option caps:swapescape"
        spawnOnce "xset r rate 300 50"

myKeys :: [(String, X ())]
myKeys =
--Browser
        [("M-b b",        spawn "brave")
        ,("M-b S-b",      spawn "brave --incognito")
        ,("M-b f",        spawn "firefox")
        ,("M-b S-f",      spawn "firefox --private-window")
        ,("M-b v",        spawn "vivaldi-stable")
        ,("M-b S-v",      spawn "vivaldi-stable")
        ,("M-b q",        spawn "qutebrowser")
        ,("M-b t",        spawn "thunderbird")
        ,("M-b y",        spawn "freetube")
        ,("M-b s",        spawn "spotify-launcher")
--Other
        ,("M-a",          spawn "thunar")
        ,("M-z",          spawn ( myTerminal ++ " -e ranger "))
        ,("M-S-z",        spawn ( myTerminal ++ " -e sudo ranger "))
        ,("C-M1-h",       spawn "clipmenu")
        -- ,("M-C-o",        unGrab *> spawn "scrot -s")

--Emacs
        ,("M-c e",        spawn myEmacs)
        ,("M-c d",        spawn (myEmacs ++ "--eval '(dired nil)'"))
        ,("M-c m",        spawn (myEmacs ++ "--eval '(mu4e)'"))
        ,("M-c u",        spawn (myEmacs ++ "--eval '(list-packages)'"))
--Audio
        ,("<XF86AudioMute>",            spawn "pamixer -t")
        ,("<XF86AudioRaiseVolume>",     spawn "pamixer -i 5")
        ,("<XF86AudioLowerVolume>",     spawn "pamixer -d 5")
        ,("<XF86AudioMicMute>",         spawn "pactl set-source-mute 1 toggle")
        ,("S-<F3>",                     spawn "pactl set-sink-volume 0 +5%")
        ,("S-<F2>",                     spawn "pactl set-sink-volume 0 -5%")
--Scratchpad
        ,("M-C-<Return>",        namedScratchpadAction myScratchPads "terminal")
        ,("M-q",                 namedScratchpadAction myScratchPads "calculator")
        ,("M-c s",               namedScratchpadAction myScratchPads "emacs-scratch")
-- Window resizing
        ,("M-h",    sendMessage Shrink)                   -- Shrink horiz window width
        ,("M-l",    sendMessage Expand)                   -- Expand horiz window width
        ,("M-f",    sendMessage (MT.Toggle FULL) >> sendMessage ToggleStruts)
        ,("M-M1-j", sendMessage MirrorShrink)             -- Shrink vert window width
        ,("M-M1-k", sendMessage MirrorExpand)             -- Exoand vert window width
--Dmenu/Scripts
        ,("M-p p",      spawn ".config/scripts/power.sh")
        ,("M-p x",      spawn ".config/scripts/xmonad.sh")
        ,("M-p c",      spawn ".config/scripts/configs.sh")
        ,("M-p k",      spawn ".config/scripts/kill.sh")
        ,("M-p a",      spawn ".config/scripts/audio.sh")
        ,("M-p s",      spawn ".config/scripts/search.sh")
        ,("M-p S-s",      spawn ".config/scripts/search.sh -p")
        ,("M-p d",      spawn "dmenu_run")
        ,("M-p r",      spawn "rofi -show drun -icon-theme \"Papirus\" -show-icons")
--Easy Motion
        ,("M-s", selectWindow def{txtCol="Green", cancelKey=xK_Escape, emFont="xft: Sans-40", overlayF=textSize} >>= (`whenJust` windows . W.focusWindow))
        ,("M-x", selectWindow def{txtCol="Red", cancelKey=xK_Escape, emFont="xft: Sans-40", overlayF=textSize} >>= (`whenJust` killWindow))
-- Tagging
        ,("M-C-a", windows copyToAll)   -- copy window to all workspaces
        ,("M-C-q", killAllOtherCopies)  -- kill copies of window on other workspaces
        ,("M-C-c", kill1)               -- kill only this copy, not every window 
        ]
        ++
-- DWM like tagging
        [("M-" ++ m ++ k, windows $ f i)
            | (i, k) <- zip myWorkspaces (map show [1 :: Int ..])
            , (f, m) <- [(W.greedyView, ""), (W.shift, "S-"), (copy, "M-C-")]]

    -- The following lines are needed for named scratchpads.
    -- where nonNSP          = WSIs (return (\ws -> W.tag ws /= "NSP"))
    --       nonEmptyNonNSP  = WSIs (return (\ws -> isJust (W.stack ws) && W.tag ws /= "NSP"))



myScratchPads :: [NamedScratchpad]
myScratchPads = [ NS "terminal" spawnTerm findTerm manageTerm
                , NS "calculator" spawnCalc findCalc manageCalc
                , NS "emacs-scratch" spawnEmacs findEmacs manageEmacs
                ]
  where
    spawnTerm  = myTerminal ++ " -T scratchpad"
    findTerm   = title =? "scratchpad"
    manageTerm = customFloating $ W.RationalRect l t w h
               where
                 h = 0.9
                 w = 0.9
                 t = 0.95 -h
                 l = 0.95 -w
    spawnCalc  = "qalculate-gtk"
    findCalc   = className =? "Qalculate-gtk"
    manageCalc = customFloating $ W.RationalRect l t w h
               where
                 h = 0.5
                 w = 0.4
                 t = 0.75 -h
                 l = 0.70 -w
    spawnEmacs  = "emacsclient -a='' -nc --frame-parameters='(quote (name . \"emacs-scratch\"))'"
    findEmacs   = title =? "emacs-scratch"
    manageEmacs = customFloating $ W.RationalRect l t w h
               where
                 h = 0.9
                 w = 0.9
                 t = 0.95 -h
                 l = 0.95 -w

myWorkspaces = ["1","2","3","4","5","6","7","8","9"]

myManageHook = composeAll
        [ className =? "KeePassXC"     --> doShift "7"
        -- , className =? "Emacs"         --> doShift "4"
        , className =? "Thunderbird"   --> doShift "7"
        , className =? "FreeTube"      --> doShift "8"
        , className =? "mpv"           --> doShift "9"
--        , className =? "Qalculate-gtk" --> doFloat
        , manageDocks
        ] <+> namedScratchpadManageHook myScratchPads


myLayoutHook = avoidStruts  $ smartBorders $  spacingRaw True (Border 0 6 6 6) True (Border 6 6 6 6) True $ mkToggle(FULL ?? EOT) myDefaultLayout 

myDefaultLayout = tiled ||| threeCol ||| Full
    where
      threeCol = magnifiercz' 1.8 $ ThreeColMid nmaster delta ratio
      tiled    = Tall nmaster delta ratio
      nmaster  = 1
      ratio    = 1/2
      delta    = 3/100

mySB = statusBarProp "xmobar" (pure xmobarPP)

main :: IO ()
main = do
  
    -- xmproc <- spawnPipe "xmobar"
    xmonad $ withSB mySB $ ewmhFullscreen $ ewmh $ docks def
        { layoutHook = myLayoutHook 
        , modMask       = mod4Mask
        , borderWidth   = 4
        , manageHook    = myManageHook <+> manageHook def
        , logHook       = dynamicLogWithPP xmobarPP
                        { ppTitle = xmobarColor "blue" "" . shorten 100
                         }
                        -- -- , ppOutput = hPutStrLn xmproc  
                        -- , ppCurrent = xmobarColor "green" "" . wrap
                        -- "<box type=Bottom width=2 mt=2 color=green >" "</box>"
                        -- , ppVisible = xmobarColor "green" "" 
                        -- , ppHidden = xmobarColor "green" ""
                        -- } 
                        >> updatePointer (0.005, 0.01) (0, 0)
        , terminal      = myTerminal
        , startupHook   = myStartupHook
        , focusedBorderColor = "#8A2BE2"
        , workspaces    = ["1","2","3","4","5","6","7","8","9"]
        } `additionalKeysP` myKeys

desktop                _= desktopConfig
