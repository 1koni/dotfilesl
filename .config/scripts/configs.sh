#!/bin/sh

#term="kitty"

chosen=$(printf "Alacritty\nEmacs\nHyprland\nNeoVim\nPicom\nRanger\nRifle(Ranger)\nWaybar\nXMobar\nXMonad\nZsh" | dmenu -i -p "Configs")

if [ "$chosen" = "Alacritty" ]; then
#	$term -e nvim ~/.config/alacritty/alacritty.yml 
	emacsclient -c -a 'emacs' ~/.dotfilesl/.config/alacritty/alacritty.yml
elif [ "$chosen" = "Emacs" ]; then
#	$term -e nvim ~/.config/kitty/kitty.conf
	emacsclient -c -a 'emacs' ~/.dotfiles/.emacs.d/Emacs.org
elif [ "$chosen" = "Hyprland" ]; then
#	$term -e nvim ~/.config/kitty/kitty.conf
	emacsclient -c -a 'emacs' ~/.config/hypr/hyprland.conf
elif [ "$chosen" = "Waybar" ]; then
#	$term -e nvim ~/.config/kitty/kitty.conf
	emacsclient -c -a 'emacs' ~/.config/waybar/config
#elif [ "$chosen" = "Kitty" ]; then
#	$term -e nvim ~/.config/kitty/kitty.conf
#	emacsclient -c -a 'emacs' ~/.config/kitty/kitty.conf
elif [ "$chosen" = "NeoVim" ]; then
#	$term -e nvim ~/.config/nvim/init.vim
	emacsclient	-c -a 'emacs' ~/.config/nvim/init.vim	
elif [ "$chosen" = "Picom" ]; then
#	$term -e nvim ~/.config/picom.conf
emacsclient	-c -a 'emacs' ~/.config/picom.conf	
elif [ "$chosen" = "Ranger" ]; then
#	$term -e nvim ~/.config/ranger/rc.conf
emacsclient	-c -a 'emacs' ~/.dotfiles/.config/ranger/rc.conf
elif [ "$chosen" = "Rifle(Ranger)" ]; then
#	$term -e nvim ~/.config/ranger/rifle.conf
emacsclient	-c -a 'emacs' ~/.dotfiles/.config/ranger/rifle.conf
elif [ "$chosen" = "XMobar" ]; then
#	$term -e nvim ~/.xmobarrc
emacsclient	-c -a 'emacs' ~/.dotfilesl/.xmobarrc
elif [ "$chosen" = "XMonad" ]; then
#	$term -e nvim ~/.xmonad/xmonad.hs
emacsclient	-c -a 'emacs' ~/.dotfilesl/.xmonad/xmonad.hs
elif [ "$chosen" = "Zsh" ]; then
#	$term -e nvim ~/.zshrc
emacsclient	-c -a 'emacs' ~/.dotfiles/.zshrc
fi
